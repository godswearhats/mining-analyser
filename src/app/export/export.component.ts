import { Component, OnInit } from '@angular/core';
import {JournalService, MaterialPercentile} from '../journal.service';
import {Body, StarSystem} from '../statistic/statistic.component';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss']
})
export class ExportComponent implements OnInit {
  public starSystems: StarSystem[] = [];
  public bodies: Body[] = [];
  public commodities: string[] = [];
  public inProgress = false;

  constructor(
    private journal: JournalService,
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    for (const runIndex in this.journal.miningRuns) {
      if (this.journal.miningRuns.hasOwnProperty(runIndex)) {
        const run = this.journal.miningRuns[runIndex];

        if (!this.starSystems.find((v) => v.Name === run.location.starSystem)) {
          this.starSystems.push({
            Address: run.location.systemAddress,
            Name: run.location.starSystem,
          });
        }

        if (!this.bodies.find((v) => v.Name === run.location.body)) {
          this.bodies.push({
            Address: run.location.systemAddress,
            ID: run.location.bodyID,
            Name: run.location.body,
          });
        }

        for (const commodity in run.prospectedAsteroidsMaterialCount) {
          if (!this.commodities.find((v) => v === commodity)) {
            this.commodities.push(commodity);
          }
        }
      }
    }

    this.starSystems.sort((a, b) => {
      if (a.Name < b.Name) { return -1; }
      if (a.Name > b.Name) { return 1; }
      return 0;
    });

    this.bodies.sort((a, b) => {
      if (a.Name < b.Name) { return -1; }
      if (a.Name > b.Name) { return 1; }
      return 0;
    });
  }

  public exportJournal(starAddress: number, bodyID: number, typ: string): void {
    let filename = 'journal.json';
    const starSystem = this.starSystems.find((v) => v.Address === starAddress);
    const body = this.bodies.find((v) => v.Address === starAddress && v.ID === bodyID);

    if (typ === 'prospector') {
      filename = `prospected-asteroids${starSystem ? '-' + starSystem.Name : ''}${body ? '-' + body.Name : ''}.json`;
    }

    const prospectedAsteroidDuplicateCheck: {[key: string]: boolean} = {};
    let lastLocation;
    const events = [];

    for (const content of this.journal.eventsSorted()) {
      if (content.event === 'SupercruiseExit' || content.event === 'Location') {
        lastLocation = content;
      }
      if ((content.event === 'StartJump' || content.event === 'Died')) {
        lastLocation = undefined;
      }

      if (starAddress > -1 && (!lastLocation || lastLocation.SystemAddress !== starAddress)) {
        continue;
      }
      if (bodyID > -1 && (!lastLocation || lastLocation.BodyID !== bodyID)) {
        continue;
      }

      if (typ === 'prospector') {
        if (content.event === 'ProspectedAsteroid') {
          const checksum = (lastLocation ? lastLocation.BodyID : '') + this.journal.prospectorChecksum(content);
          if (prospectedAsteroidDuplicateCheck[checksum]) {
            continue;
          } else {
            prospectedAsteroidDuplicateCheck[checksum] = true;
          }

          events.push(JSON.stringify(content));
        }
      }
    }

    const journal = events.join('\n');
    this.downloadFile(journal, filename, 'text/plain;charset=utf-8;');
  }

  public exportCSV(starAddress: number, bodyID: number, typ: string): void {
    const headers: string[] = [];
    const items: any[][] = [];
    let filename = 'export.csv';
    const starSystem = this.starSystems.find((v) => v.Address === starAddress);
    const body = this.bodies.find((v) => v.ID === bodyID);

    const prospectedAsteroidDuplicateCheck: {[key: string]: boolean} = {};

    if (typ === 'prospector') {
      filename = `prospected-asteroids${starSystem ? '-' + starSystem.Name : ''}${body ? '-' + body.Name : ''}.csv`;

      for (const content of this.journal.eventsSorted()) {
        if (typ === 'prospector' && content.event === 'ProspectedAsteroid') {
          for (const material of content.Materials) {
            const name = material.Name_Localised || material.Name;
            if (!headers.find((v) => v === name)) {
              headers.push(name);
            }
          }
        }
      }
      headers.sort();
    }

    let lastLocation;
    for (const content of this.journal.eventsSorted()) {
      if (content.event === 'SupercruiseExit' || content.event === 'Location') {
        lastLocation = content;
      }
      if ((content.event === 'StartJump' || content.event === 'Died')) {
        lastLocation = undefined;
      }

      if (starAddress > -1 && (!lastLocation || lastLocation.SystemAddress !== starAddress)) {
        continue;
      }
      if (bodyID > -1 && (!lastLocation || lastLocation.BodyID !== bodyID)) {
        continue;
      }


      if (typ === 'prospector') {
        if (content.event === 'ProspectedAsteroid') {
          const row: number[] = [];

          const checksum = (lastLocation ? lastLocation.BodyID : '') + this.journal.prospectorChecksum(content);
          if (prospectedAsteroidDuplicateCheck[checksum]) {
            continue;
          } else {
            prospectedAsteroidDuplicateCheck[checksum] = true;
          }

          for (const head of headers) {
            let proportion = 0;

            for (const material of content.Materials) {
              const name = material.Name_Localised || material.Name;
              if (name === head) {
                proportion = material.Proportion;
                break;
              }
            }

            row.push(proportion);
          }

          items.push(row);
        }
      }
    }

    const csv = this.createCSV(headers, items);
    this.downloadFile(csv, filename, 'text/csv;charset=utf-8;');
  }

  public transmitGalaxy(): void {
    let journal = '';
    for (const content of this.journal.eventsSorted()) {
      journal += JSON.stringify(content) + '\n';
    }

    if (!journal) {
      this.snackBar.open('No journal to transmit');
      return;
    }

    this.inProgress = true;
    this.snackBar.open('Journal transiting ....', '', {
      duration: 5 * 1000,
    });

    this.http.post('//edgalaxy.fankserver.com/import/journal', journal, { responseType: 'text'}).subscribe(
      () => {
        this.snackBar.open('Journal successful transmitted', '', {
          duration: 5 * 1000,
        });
        this.inProgress = false;
      },
      (err) => {
        this.snackBar.open('Error transmitting', '', {
          duration: 30 * 1000,
        });
        this.inProgress = false;
      },
    );
  }

  public exportNorm(c: string) {
    const filename = 'norm_CDFb_' + c + '.json';

    const asteroidMaterialCumulativeFrequencyChartDataCDFb: {[key: string]: any[]} = {};
    const asteroidMaterialCumulativeFrequencyChartDataCDF: {[key: string]: any[]} = {};

    const commodityPercentile: {[key: string]: MaterialPercentile[]} = {};
    let prospectedAsteroidCount = 0;

    for (const runIndex in this.journal.miningRuns) {
      if (this.journal.miningRuns.hasOwnProperty(runIndex)) {
        const run = this.journal.miningRuns[runIndex];

        prospectedAsteroidCount += run.prospectedAsteroidCount;

        for (const entry of run.prospectedAsteroidMaterialPercentiles) {
          if (!commodityPercentile[entry.name]) {
            commodityPercentile[entry.name] = [];
          }

          let percentileFound = false;
          for (const categoryIndex in commodityPercentile[entry.name]) {
            if (entry.percentile === commodityPercentile[entry.name][categoryIndex].percentile) {
              commodityPercentile[entry.name][categoryIndex].amount += entry.amount;
              percentileFound = true;
              break;
            }
          }
          if (!percentileFound) {
            commodityPercentile[entry.name].push(JSON.parse(JSON.stringify(entry)));
          }
        }
      }
    }

    for (const commodity in commodityPercentile) {
      if (commodityPercentile.hasOwnProperty(commodity)) {
        asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity] = [];
        asteroidMaterialCumulativeFrequencyChartDataCDF[commodity] = [];

        for (const entry of commodityPercentile[commodity]) {
          // cumulative frequency CDFb
          let cumulatedAmountCDFb = 0;
          for (const innerEntry of commodityPercentile[commodity]) {
            if (innerEntry.percentile > entry.percentile) {
              cumulatedAmountCDFb += innerEntry.amount;
            }
          }
          asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity].push({
            name: entry.percentile,
            value: cumulatedAmountCDFb / prospectedAsteroidCount,
          });

          // cumulative frequency CDF
          asteroidMaterialCumulativeFrequencyChartDataCDF[commodity].push({
            name: entry.percentile,
            value: 1 - (cumulatedAmountCDFb / prospectedAsteroidCount),
          });
        }
      }
    }

    this.downloadFile(JSON.stringify(asteroidMaterialCumulativeFrequencyChartDataCDFb[c]), `norm_CDFb_${c}.json`, 'text/plain;charset=utf-8;');
    this.downloadFile(JSON.stringify(asteroidMaterialCumulativeFrequencyChartDataCDF[c]), `norm_CDF_${c}.json`, 'text/plain;charset=utf-8;');
  }

  private createCSV(headers: string[], items: string[][]): string {
    if (headers) {
      items.unshift(headers);
    }

    let csv = '';

    for (const item of items) {
      let line = '';

      for (const index in item) {
        if (item.hasOwnProperty(index)) {
          if (line !== '') {
            line += ',';
          }

          line += item[index];
        }
      }

      csv += line + '\r\n';
    }

    return csv;
  }

  private downloadFile(content: string, filename: string, type: string) {
    const exportedFilename = filename;

    const blob = new Blob([content], { type });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilename);
    } else {
      const link = document.createElement('a');
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', exportedFilename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
}
