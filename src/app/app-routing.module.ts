import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SelectorComponent} from './journal/selector/selector.component';
import {DetailComponent} from './journal/detail/detail.component';
import {StatisticComponent} from './statistic/statistic.component';
import {ExportComponent} from './export/export.component';
import {ImportComponent} from './import/import.component';
import {FAQComponent} from './faq/faq.component';
import {LogComponent} from './log/log.component';
import {TestComponent} from './test/test.component';
import {LocationsComponent} from './locations/locations.component';

const routes: Routes = [
  { path: '', component: SelectorComponent, },
  { path: 'detail/:id', component: DetailComponent, },
  { path: 'statistic', component: StatisticComponent, },
  { path: 'export', component: ExportComponent, },
  { path: 'import', component: ImportComponent, },
  { path: 'faq', component: FAQComponent, },
  { path: 'locations', component: LocationsComponent, },
  { path: 'logs', component: LogComponent, },
  { path: 'test', component: TestComponent, },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
