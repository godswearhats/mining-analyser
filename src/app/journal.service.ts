import { Injectable } from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface Event {
  timestamp: string;
  type: string;
  amount: number;
}

export interface MaterialPercentile {
  name: string;
  percentile: number;
  amount: number;
}

export interface ProspectedAsteroid {
  timestamp: string;
  amount: number;
}

export interface MiningRun {
  commander: string;
  ship: string;
  id: number;
  dateStart: string;
  dateEnd?: string;
  duration?: number;
  eventCount: number;
  prospectedAsteroidCount: number;
  prospectedAsteroidDuplicateCheck: {[key: string]: boolean};
  prospectedAsteroidDuplicateCount: number;
  prospectedAsteroidsMaterialCount: {[key: string]: number};
  highestProspectedAsteroidsPercentile: {[key: string]: number};
  prospectedAsteroidMaterialPercent: {[key: string]: number[]};
  prospectedAsteroidMaterialPercentiles: MaterialPercentile[];
  refinedMining: {[key: string]: number};
  ejectCargoCount: {[key: string]: number};
  materialCollectedCount: {[key: string]: number};
  launchedCollections: number;
  launchedProspectors: number;
  attacked: boolean;
  died: boolean;
  location: Location;
  gameMode: string;
  group?: string;
  loadGameCount: number;
  events: Event[];
  journal: string[];
}

export interface Location {
  starSystem: string;
  systemAddress: number;
  body: string;
  bodyID: number;
  bodyType: string;
}

export interface AnalyzeOption {
  ignoreStartAndEnd?: boolean;
  translate?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class JournalService {
  constructor(private snackBar: MatSnackBar) {}

  public miningRuns: MiningRun[] = [];
  private events: any[] = [];
  private activeRunSubject$: ReplaySubject<MiningRun> = new ReplaySubject<MiningRun>();
  public activeRun$: Observable<MiningRun> = this.activeRunSubject$ as Observable<MiningRun>;

  public reset() {
    this.miningRuns = [];
    this.events = [];
  }

  public newEvent(event: string) {
    try {
      this.events.push(JSON.parse(event));
    } catch (e) {
      console.error(e, event);
    }
  }

  public selectRun(index: number): boolean {
    if (this.miningRuns.length <= index) {
      return false;
    }
    this.activeRunSubject$.next(this.miningRuns[index]);
    return true;
  }

  public eventsSorted(): any[] {
    return this.events.sort((a, b) => {
      const aDate = Date.parse(a.timestamp);
      const bDate = Date.parse(b.timestamp);

      if (aDate < bDate) { return -1; }
      if (aDate > bDate) { return 1; }
      return 0;
    });
  }

  public analyze(option: AnalyzeOption = {}) {
    let miningRunIndex = -1;
    let lastLocation;
    let lastLoadGame;
    let lastTimestamp;
    let lastCommander;
    let lastLoadout;
    // let wingMember: string[] = [];
    // let crewMember: string[] = [];

    this.miningRuns = [];

    this.eventsSorted().forEach((content) => {
      lastTimestamp = content.timestamp;
      if (miningRunIndex > -1) {
        this.miningRuns[miningRunIndex].eventCount++;
      }

      if (content.event === 'Commander') {
        lastCommander = content;
      }
      if (content.event === 'Loadout') {
        lastLoadout = content;
      }

      if (content.event === 'LoadGame') {
        lastLoadGame = content;
        if (miningRunIndex > -1) {
          this.miningRuns[miningRunIndex].loadGameCount++;
          this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
        }
      }
      if (content.event === 'SupercruiseExit' || content.event === 'Location') {
        lastLocation = content;
      }

      /*
      if (content.event === 'WingJoin') {
        wingMember = content.Others;
      }
      if (content.event === 'WingAdd') {
        if (!wingMember.includes(content.Name)) {
          wingMember.push(content.Name);
        }
      }
      if (content.event === 'WingLeave') {
        wingMember = [];
      }

      if (content.event === 'CrewMemberJoins') {
        if (!crewMember.includes(content.Name)) {
          crewMember.push(content.Name);
        }
      }
      if (content.event === 'CrewMemberQuits') {
        crewMember = crewMember.filter((v) => v !== content.Crew);
      }
      if (content.event === 'EndCrewSession') {
        crewMember = [];
      }
      */

      if (content.event === 'LaunchDrone') {
        // start mining
        if (miningRunIndex === -1) {
          console.log('miningStart', content);

          this.miningRuns.push({
            id: this.miningRuns.length,
            commander: (lastCommander ? lastCommander.Name : 'Unknown'),
            ship: (lastLoadout ? `${lastLoadout.ShipName} (${lastLoadout.ShipIdent})` : 'Unknown'),
            eventCount: 0,
            dateStart: content.timestamp,
            prospectedAsteroidCount: 0,
            prospectedAsteroidDuplicateCheck: {},
            prospectedAsteroidDuplicateCount: 0,
            highestProspectedAsteroidsPercentile: {},
            prospectedAsteroidsMaterialCount: {},
            prospectedAsteroidMaterialPercentiles: [],
            prospectedAsteroidMaterialPercent: {},
            launchedCollections: 0,
            launchedProspectors: 0,
            refinedMining: {},
            ejectCargoCount: {},
            materialCollectedCount: {},
            attacked: false,
            died: false,
            location: {
              body: (lastLocation ? lastLocation.Body : 'Unknown'),
              bodyID: (lastLocation ? lastLocation.BodyID : -1),
              bodyType: (lastLocation ? lastLocation.BodyType : ''),
              starSystem: (lastLocation ? lastLocation.StarSystem : 'Unknown'),
              systemAddress: (lastLocation ? lastLocation.SystemAddress : -1),
            },
            gameMode: (lastLoadGame ? lastLoadGame.GameMode : 'Unknown'),
            group: (lastLoadGame ? lastLoadGame.Group : 'Unknown'),
            loadGameCount: 0,
            events: [],
            journal: [],
          });
          miningRunIndex = this.miningRuns.length - 1;

          this.miningRuns[miningRunIndex].journal.push(JSON.stringify(lastLocation));
        }

        let amount = 0;
        if (content.Type === 'Collection') {
          amount = ++this.miningRuns[miningRunIndex].launchedCollections;
        } else if (content.Type === 'Prospector') {
          amount = ++this.miningRuns[miningRunIndex].launchedProspectors;
        }

        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Launched ' + content.Type,
          amount,
        });
        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Launched Total',
          amount: this.miningRuns[miningRunIndex].launchedCollections + this.miningRuns[miningRunIndex].launchedProspectors,
        });

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      if (content.event === 'Cargo' && content.Vessel === 'Ship' && miningRunIndex > -1) {
        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Cargo',
          amount: content.Count,
        });

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      if (content.event === 'ProspectedAsteroid' && miningRunIndex > -1) {
        this.miningRuns[miningRunIndex].prospectedAsteroidCount++;
        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Prospected asteroids',
          amount: this.miningRuns[miningRunIndex].prospectedAsteroidCount,
        });

        const checksum = this.prospectorChecksum(content);
        if (this.miningRuns[miningRunIndex].prospectedAsteroidDuplicateCheck[checksum]) {
          this.miningRuns[miningRunIndex].prospectedAsteroidDuplicateCount++;
        } else {
          this.miningRuns[miningRunIndex].prospectedAsteroidDuplicateCheck[checksum] = true;

          for (const material of content.Materials) {
            const name = (option.translate ? this.translateCommodity(material.Name) : this.capitalize(material.Name_Localised || material.Name));

            this.miningRuns[miningRunIndex].prospectedAsteroidsMaterialCount[name] = this.miningRuns[miningRunIndex].prospectedAsteroidsMaterialCount[name] != null ? this.miningRuns[miningRunIndex].prospectedAsteroidsMaterialCount[name] + 1 : 1;

            let percentileFound = false;
            for (const percentile in this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles) {
              if (this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles[percentile].name === name && this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles[percentile].percentile === Math.round(material.Proportion)) {
                this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles[percentile].amount++;
                percentileFound = true;
                break;
              }
            }
            if (!percentileFound) {
              this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles.push({
                name,
                percentile: Math.round(material.Proportion),
                amount: 1,
              });
            }

            if (!this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercent[name]) {
              this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercent[name] = [];
            }
            this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercent[name].push(material.Proportion);

            if (!this.miningRuns[miningRunIndex].highestProspectedAsteroidsPercentile[name] || this.miningRuns[miningRunIndex].highestProspectedAsteroidsPercentile[name] < material.Proportion) {
              this.miningRuns[miningRunIndex].highestProspectedAsteroidsPercentile[name] = material.Proportion;
            }
          }
        }

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      if (content.event === 'MiningRefined' && miningRunIndex > -1) {
        const type = (option.translate ? content.Type : this.capitalize(content.Type_Localised || content.Type));
        this.miningRuns[miningRunIndex].refinedMining[type] = this.miningRuns[miningRunIndex].refinedMining[type] != null ? this.miningRuns[miningRunIndex].refinedMining[type] + 1 : 1;
        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Refined ' + type,
          amount: this.miningRuns[miningRunIndex].refinedMining[type],
        });

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      if (content.event === 'EjectCargo' && miningRunIndex > -1) {
        const type = (option.translate ? content.Type : this.capitalize(content.Type_Localised || content.Type));
        this.miningRuns[miningRunIndex].ejectCargoCount[type] = this.miningRuns[miningRunIndex].ejectCargoCount[type] != null ? this.miningRuns[miningRunIndex].ejectCargoCount[type] + content.Count : content.Count;
        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Ejected ' + type,
          amount: this.miningRuns[miningRunIndex].ejectCargoCount[type],
        });

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      if (content.event === 'MaterialCollected' && miningRunIndex > -1) {
        const name = (option.translate ? content.Name : this.capitalize(content.Name_Localised || content.Name));
        this.miningRuns[miningRunIndex].materialCollectedCount[name] = this.miningRuns[miningRunIndex].materialCollectedCount[name] != null ? this.miningRuns[miningRunIndex].materialCollectedCount[name] + content.Count : content.Count;
        this.miningRuns[miningRunIndex].events.push({
          timestamp: content.timestamp,
          type: 'Collected ' + name,
          amount: this.miningRuns[miningRunIndex].materialCollectedCount[name],
        });

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      if (content.event === 'UnderAttack' && miningRunIndex > -1) {
        this.miningRuns[miningRunIndex].attacked = true;

        this.miningRuns[miningRunIndex].journal.push(JSON.stringify(content));
      }

      // end mining
      if ((content.event === 'StartJump' || content.event === 'Died') && miningRunIndex > -1 && !option.ignoreStartAndEnd) {
        console.log('miningEnd', content);

        this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles.sort((a, b) => {
          if (a.percentile < b.percentile) {
            return -1;
          } else if (a.percentile > b.percentile) {
            return 1;
          } else {
            return 0;
          }
        });

        if (content.event === 'Died') {
          this.miningRuns[miningRunIndex].died = true;
        }

        this.miningRuns[miningRunIndex].dateEnd = content.timestamp;
        this.miningRuns[miningRunIndex].duration = Date.parse(this.miningRuns[miningRunIndex].dateEnd) - Date.parse(this.miningRuns[miningRunIndex].dateStart);

        // cleanup
        this.miningRuns[miningRunIndex].prospectedAsteroidDuplicateCheck = undefined;
        miningRunIndex = -1;
        lastLocation = undefined;
      }
    });

    // end mining
    if (miningRunIndex > -1) {
      this.miningRuns[miningRunIndex].prospectedAsteroidMaterialPercentiles.sort((a, b) => {
        if (a.percentile < b.percentile) {
          return -1;
        } else if (a.percentile > b.percentile) {
          return 1;
        } else {
          return 0;
        }
      });

      this.miningRuns[miningRunIndex].dateEnd = lastTimestamp;
      this.miningRuns[miningRunIndex].duration = Date.parse(this.miningRuns[miningRunIndex].dateEnd) - Date.parse(this.miningRuns[miningRunIndex].dateStart);

      // cleanup
      this.miningRuns[miningRunIndex].prospectedAsteroidDuplicateCheck = undefined;
      miningRunIndex = -1;
      lastLocation = undefined;
    }

    this.snackBar.open('Import complete', '', {
      duration: 5 * 1000,
    });
  }

  public prospectorChecksum(event: any): string {
    let checksum = event.Content;

    const materials = [].concat(event.Materials).sort((a, b) => {
      if (a.Name < b.Name) { return -1; }
      if (a.Name > b.Name) { return 1; }
      return 0;
    });
    for (const material of materials) {
      checksum += `;${material.Name}:${material.Proportion}`;
    }

    return checksum;
  }

  public translateCommodity(name: string): string {
    switch (name) {
      case 'LowTemperatureDiamond':
        return 'Low Temperature Diamonds';
      case 'tritium':
        return 'Tritium';
      default:
        return this.capitalize(name);
    }
  }

  public capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }
}
