import { Component, OnInit } from '@angular/core';
import {JournalService, MaterialPercentile} from '../journal.service';
import {norms} from '../norms';

export interface GameMode {
  Name: string;
  Value: string;
}
export interface StarSystem {
  Address: number;
  Name: string;
}
export interface Body {
  Address: number;
  ID: number;
  Name: string;
}

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {
  public objectKeys = Object.keys;
  get cumulativeFrequencyChartType(): string {
    return localStorage.getItem('Statistic_CumulativeFrequencyChartType') || 'CDFb';
  }
  set cumulativeFrequencyChartType(value: string) {
    localStorage.setItem('Statistic_CumulativeFrequencyChartType', value);
  }

  public asteroidMaterialPercentCharData: {[key: string]: any[]} = {};
  public asteroidMaterialCumulativeFrequencyChartDataCDFb: {[key: string]: any[]} = {};
  public asteroidMaterialCumulativeFrequencyChartDataCDF: {[key: string]: any[]} = {};
  public prospectedAsteroidsMaterialCount: {[key: string]: number} = {};
  public asteroidMaterialPercentilesCharData: any[] = [];
  public materialStats: any[] = [];
  public eventCount = 0;
  public prospectedAsteroidCount = 0;
  public launchedProspectors = 0;
  public launchedCollections = 0;

  public gameModes: GameMode[] = [];
  public filterGameMode = '';
  public starSystems: StarSystem[] = [];
  public filterStarSystem = -1;
  public bodies: Body[] = [];
  public filterBody: Body;

  constructor(
    private journal: JournalService,
  ) {}

  ngOnInit(): void {
    if (this.starSystems.length === 0) {
      this.gameModes = [];
      this.starSystems = [];
      this.bodies = [];

      for (const runIndex in this.journal.miningRuns) {
        if (this.journal.miningRuns.hasOwnProperty(runIndex)) {
          const run = this.journal.miningRuns[runIndex];

          if (!this.gameModes.find((v) => v.Name === run.gameMode)) {
            this.gameModes.push({ Name: run.gameMode, Value: run.gameMode });
          }

          if (!this.starSystems.find((v) => v.Name === run.location.starSystem)) {
            this.starSystems.push({
              Address: run.location.systemAddress,
              Name: run.location.starSystem,
            });
          }

          if (!this.bodies.find((v) => v.Name === run.location.body)) {
            this.bodies.push({
              Address: run.location.systemAddress,
              ID: run.location.bodyID,
              Name: run.location.body,
            });
          }
        }
      }

      this.starSystems.sort((a, b) => {
        if (a.Name < b.Name) { return -1; }
        if (a.Name > b.Name) { return 1; }
        return 0;
      });

      this.bodies.sort((a, b) => {
        if (a.Name < b.Name) { return -1; }
        if (a.Name > b.Name) { return 1; }
        return 0;
      });

      this.gameModes.unshift({Name: 'All', Value: ''});
      this.starSystems.unshift({Name: 'All', Address: -1});
      this.bodies.unshift({Name: 'All', Address: -1, ID: -1});

      this.filterBody = this.bodies[0];
    }

    this.analyse();
  }

  public analyse(): void {
    this.materialStats = [];
    this.asteroidMaterialPercentCharData = {};
    this.asteroidMaterialPercentilesCharData = [];
    this.eventCount = 0;
    this.prospectedAsteroidCount = 0;
    this.launchedProspectors = 0;
    this.launchedCollections = 0;

    const commodityPercentile: {[key: string]: MaterialPercentile[]} = {};
    const highestPercentile: {[key: string]: any} = {};
    const prospectedAsteroidMaterialPercent: {[key: string]: number[]} = {};
    let prospectedAsteroidCount = 0;

    for (const runIndex in this.journal.miningRuns) {
      if (this.journal.miningRuns.hasOwnProperty(runIndex)) {
        const run = this.journal.miningRuns[runIndex];

        this.eventCount += run.eventCount;
        this.prospectedAsteroidCount += run.prospectedAsteroidCount;
        this.launchedProspectors += run.launchedProspectors;
        this.launchedCollections += run.launchedCollections;

        if (this.filterGameMode !== '' && run.gameMode !== this.filterGameMode) {
          continue;
        }
        if (this.filterStarSystem > -1 && run.location.systemAddress !== this.filterStarSystem) {
          continue;
        }
        if (this.filterBody && this.filterBody.ID > -1 && run.location.systemAddress !== this.filterBody.Address && run.location.bodyID !== this.filterBody.ID) {
          continue;
        }

        prospectedAsteroidCount += run.prospectedAsteroidCount;

        for (const material in run.prospectedAsteroidMaterialPercent) {
          if (run.prospectedAsteroidMaterialPercent.hasOwnProperty(material)) {
            if (!prospectedAsteroidMaterialPercent[material]) {
              prospectedAsteroidMaterialPercent[material] = [];
            }

            prospectedAsteroidMaterialPercent[material] = prospectedAsteroidMaterialPercent[material].concat(run.prospectedAsteroidMaterialPercent[material]);
          }
        }

        for (const entry of run.prospectedAsteroidMaterialPercentiles) {
          if (!commodityPercentile[entry.name]) {
            commodityPercentile[entry.name] = [];
          }

          let percentileFound = false;
          for (const categoryIndex in commodityPercentile[entry.name]) {
            if (entry.percentile === commodityPercentile[entry.name][categoryIndex].percentile) {
              commodityPercentile[entry.name][categoryIndex].amount += entry.amount;
              percentileFound = true;
              break;
            }
          }
          if (!percentileFound) {
            commodityPercentile[entry.name].push(JSON.parse(JSON.stringify(entry)));
          }
        }

        for (const material in run.highestProspectedAsteroidsPercentile) {
          if (run.highestProspectedAsteroidsPercentile.hasOwnProperty(material)) {
            if (!highestPercentile[material] || highestPercentile[material].highest < run.highestProspectedAsteroidsPercentile[material]) {
              highestPercentile[material] = {
                highest: run.highestProspectedAsteroidsPercentile[material],
                runIndex,
              };
            }
          }
        }

        for (const commodity in run.prospectedAsteroidsMaterialCount) {
          if (run.prospectedAsteroidsMaterialCount.hasOwnProperty(commodity)) {
            if (!this.prospectedAsteroidsMaterialCount[commodity]) {
              this.prospectedAsteroidsMaterialCount[commodity] = 0;
            }
            this.prospectedAsteroidsMaterialCount[commodity] += run.prospectedAsteroidsMaterialCount[commodity];
          }
        }
      }
    }

    for (const commodity in commodityPercentile) {
      if (commodityPercentile.hasOwnProperty(commodity)) {
        this.asteroidMaterialPercentCharData[commodity] = [];
        this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity] = [];
        this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity] = [];

        const percentSeries: any[] = [];
        let highest = 0;
        const amounts: {[key: string]: boolean} = {};
        for (const entry of commodityPercentile[commodity]) {
          percentSeries.push({
            name: entry.percentile,
            y: commodity,
            x: entry.percentile,
            r: entry.amount,
          });

          this.asteroidMaterialPercentCharData[commodity].push({
            name: entry.percentile,
            value: entry.amount,
          });

          if (!highest || entry.amount > highest) {
            highest = entry.percentile;
          }
          amounts[entry.percentile] = true;

          // cumulative frequency CDFb
          let cumulatedAmountCDFb = 0;
          for (const innerEntry of commodityPercentile[commodity]) {
            if (innerEntry.percentile > entry.percentile) {
              cumulatedAmountCDFb += innerEntry.amount;
            }
          }
          this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity].push({
            name: entry.percentile,
            value: cumulatedAmountCDFb / prospectedAsteroidCount,
          });

          // cumulative frequency CDF
          this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity].push({
            name: entry.percentile,
            value: 1 - (cumulatedAmountCDFb / prospectedAsteroidCount),
          });
        }

        // cumulative frequency CDFb
        this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity] = [
          {
            name: 'x',
            series: this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity],
          }
        ];
        if (norms[commodity]) {
          for (const norm in norms[commodity]) {
            if (norms[commodity].hasOwnProperty(norm)) {
              this.asteroidMaterialCumulativeFrequencyChartDataCDFb[commodity].push({
                name: norm,
                series: norms[commodity][norm],
              });
            }
          }
        }

        // cumulative frequency CDF
        this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity] = [
          {
            name: 'x',
            series: this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity],
          }
        ];
        if (norms[commodity]) {
          for (const norm in norms[commodity]) {
            if (norms[commodity].hasOwnProperty(norm)) {
              const series = JSON.parse(JSON.stringify(norms[commodity][norm]));
              for (const entry of series) {
                entry.value = 1 - entry.value;
              }

              this.asteroidMaterialCumulativeFrequencyChartDataCDF[commodity].push({
                name: norm,
                series,
              });
            }
          }
        }

        for (let i = 1; i < highest; i++) {
          if (amounts[i]) {
            continue;
          }

          this.asteroidMaterialPercentCharData[commodity].push({
            name: i,
            value: 0,
          });
        }

        this.asteroidMaterialPercentCharData[commodity].sort((a, b) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });

        this.asteroidMaterialPercentilesCharData.push({
          name: commodity,
          series: percentSeries,
        });
      }
    }

    for (const material in highestPercentile) {
      if (highestPercentile.hasOwnProperty(material)) {
        this.materialStats.push({
          material,
          highest: highestPercentile[material].highest / 100,
          highestRun: highestPercentile[material].runIndex
        });
      }
    }


    for (const material in prospectedAsteroidMaterialPercent) {
      if (prospectedAsteroidMaterialPercent.hasOwnProperty(material)) {
        for (const i in this.materialStats) {
          if (this.materialStats[i].material !== material) {
            continue;
          }

          const total = prospectedAsteroidMaterialPercent[material].reduce((a, b) => a + b, 0);
          this.materialStats[i].average = (total / prospectedAsteroidCount) / 100;
          this.materialStats[i].amount = prospectedAsteroidMaterialPercent[material].length;
        }
      }
    }

    this.materialStats.sort((a, b) => {
      if (a.material < b.material) { return -1; }
      if (a.material > b.material) { return 1; }
      return 0;
    });
  }

  public containsData(): boolean {
    return this.journal.miningRuns.length > 0;
  }

  public percentTickFormatting(val: any): string {
    return val.toLocaleString('en-uk', {style: 'percent', maximumSignificantDigits: 1});
  }
}
