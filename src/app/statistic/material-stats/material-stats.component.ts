import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-material-stats',
  templateUrl: './material-stats.component.html',
  styleUrls: ['./material-stats.component.scss']
})
export class MaterialStatsComponent implements OnInit, OnChanges {
  public dataSource: MatTableDataSource<any>;
  public displayedColumns: string[] = ['material', 'amount', 'highest', 'highestRun', 'average'];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input() public stats: any[] = [];

  constructor() {
    this.dataSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;

    this.dataSource.sort.sort({
      id: 'material',
      start: 'asc',
      disableClear: false,
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource.data = this.stats;
  }
}
