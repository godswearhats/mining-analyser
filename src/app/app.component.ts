import {Component, Inject} from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {authConfig} from './capi/config';
import {SwUpdate} from '@angular/service-worker';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {APP_BASE_HREF} from '@angular/common';
import {JwksValidationHandler} from 'angular-oauth2-oidc-jwks';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private oauthService: OAuthService,
    private swUpdate: SwUpdate,
    private snackBar: MatSnackBar,
    @Inject(APP_BASE_HREF) private baseHref: string,
    private iconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('imu', domSanitizer.bypassSecurityTrustResourceUrl(`${this.baseHref === '/' ? '' : this.baseHref}/assets/icons/imu-logo.svg`));

    this.configure();

    this.swUpdate.available.subscribe(() => {
      console.log('update available');
      this.swUpdate.activateUpdate().then(() => {
        console.log('update activated');
        const snackbar = this.snackBar.open('An update to the app is available, please reload.', 'Reload');

        snackbar.onAction().subscribe(() => location.reload());
      });
    });

    this.swUpdate.checkForUpdate();
  }

  private configure() {
    this.oauthService.setStorage(localStorage);
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.tryLogin();
  }
}
