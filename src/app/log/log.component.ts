import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import {from} from 'rxjs';
import {concatMap, tap} from 'rxjs/operators';
import {JournalService} from '../journal.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {
  public isImporting = false;
  public dataSource: MatTableDataSource<any>;
  public selection = new SelectionModel<any>(true, []);
  public displayedColumns: string[] = ['select', 'id', 'commander', 'type', 'startedAt', 'body', 'comment'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private http: HttpClient,
    private journal: JournalService,
    private snackBar: MatSnackBar,
  ) {
    this.dataSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.dataSource.sort.sort({
      id: 'id',
      start: 'desc',
      disableClear: false,
    });

    this.http.get<any[]>('https://edgalaxy.fankserver.com/logs').subscribe((v) => {
      this.dataSource.data = v;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  // masterToggle() {
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.dataSource.data.forEach(row => this.selection.select(row));
  // }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  importSelected() {
    this.isImporting = true;

    this.journal.reset();

    from(this.selection.selected.map((v) => v.id))
      .pipe(
        tap((id) => console.log('load', id)),
        concatMap((id) =>  this.http.get('https://edgalaxy.fankserver.com/export/log/' + id, {
          responseType: 'text',
        }))
      )
      .subscribe(
      (journals) => {
        for (const line of journals.split('\n')) {
          if (line === '') {
            continue;
          }

          this.journal.newEvent(line);
        }
      },
      (err) => {
        this.snackBar.open('Error on importing logs: ' + err.message, null, {
          duration: 5000,
        });
      },
      () => {
        this.journal.analyze({
          ignoreStartAndEnd: true,
          translate: true,
        });
      }
    );

    this.isImporting = false;
  }
}
