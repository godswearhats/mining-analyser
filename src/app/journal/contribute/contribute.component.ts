import {Component, Input} from '@angular/core';
import {MiningRun} from '../../journal.service';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {SelectorComponent} from '../selector/selector.component';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-contribute',
  templateUrl: './contribute.component.html',
  styleUrls: ['./contribute.component.scss']
})
export class ContributeComponent {
  @Input() public run: MiningRun;
  public type = '';
  public comment = '';
  public loading = false;

  constructor(
    private bottomSheetRef: MatBottomSheetRef<SelectorComponent>,
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ) { }

  public dismiss() {
    this.bottomSheetRef.dismiss();
  }

  public submit() {
    this.loading = true;

    this.http.post('https://edgalaxy.fankserver.com/import/log', {
      commander: this.run.commander || 'Unknown (maybe console)',
      type: this.type,
      body: this.run.location.body,
      startedAt: this.run.dateStart,
      comment: this.comment,
      journal: this.run.journal,
    }, {
      responseType: 'text',
    }).subscribe(
      (v) => {
        this.loading = false;
        this.snackBar.open(v, null, {
          duration: 5000,
        });
        this.bottomSheetRef.dismiss();
      },
      (err) => {
        this.snackBar.open('Error on submitting journal', null, {
          duration: 5000,
        });
        console.error(err);
        this.loading = false;
      },
    );
  }
}
