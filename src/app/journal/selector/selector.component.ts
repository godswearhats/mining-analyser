import { Component } from '@angular/core';
import {JournalService, MiningRun} from '../../journal.service';
import {OAuthService} from 'angular-oauth2-oidc';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {ContributeComponent} from '../contribute/contribute.component';

@Component({
  selector: 'app-journal-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent {
  public importProgress = 0;
  public get onlyWithCollection() {
    return !!localStorage.getItem('selector_option_only_with_collection');
  }
  public set onlyWithCollection(value: boolean) {
    if (value) {
      localStorage.setItem('selector_option_only_with_collection', '1');
    } else {
      localStorage.removeItem('selector_option_only_with_collection');
    }
  }
  public get onlyWithProspector() {
    return !!localStorage.getItem('selector_option_only_with_prospectpr');
  }
  public set onlyWithProspector(value: boolean) {
    if (value) {
      localStorage.setItem('selector_option_only_with_prospectpr', '1');
    } else {
      localStorage.removeItem('selector_option_only_with_prospectpr');
    }
  }

  constructor(
    private journal: JournalService,
    private oauthService: OAuthService,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private bottomSheet: MatBottomSheet,
  ) {}

  public importCAPI(): void {
    if (this.oauthService.hasValidAccessToken()) {
      const max = 7;
      let i = 0;

      const run = () => {
        this.getJournal(i).subscribe(
          (v) => {
            if (v) {
              v = v.replace(/{ "limited":"did not reach end of file, refresh to continue" }/g, '');

              for (const line of v.split('\n')) {
                if (line === '') {
                  continue;
                }

                this.journal.newEvent(line);
              }
            }
          },
          (err) => {
            console.error(err);
            this.snackBar.open('Error getting log from ' + i + ' days ago: ' + err.statusText);
          },
          () => {
            i++;
            this.importProgress = (i / max) * 100;
            if (i <= max) {
              run();
            } else {
              this.journal.analyze();
              this.importProgress = 0;
            }
          }
        );
      };

      run();
    } else {
      this.oauthService.initLoginFlow();
    }
  }

  private getJournal(daysAgo: number): Observable<string> {
    let url = 'https://companion-orerve-hack.fankservercdn.com/journal';
    if (daysAgo > 0) {
      const d = new Date();
      d.setDate(d.getDate() - daysAgo);
      url += '/' + d.getUTCFullYear() + '/' + (d.getUTCMonth() + 1) + '/' + d.getUTCDate();
    }

    return this.http.get(url, {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
        'X-User-Agent': 'edmininganalyzer'
      }),
      responseType: 'text',
    });
  }

  public readJournals(event: any) {
    const input = event.target;

    this.journal.reset();

    let fileReadCounter = 0;
    for (const file of input.files) {
      const reader = new FileReader();
      reader.readAsText(file);
      reader.onload = () => {
        for (const line of (reader.result as string).split('\n')) {
          if (line === '') {
            continue;
          }

          this.journal.newEvent(line);
        }
      };

      reader.onloadend = () => {
        fileReadCounter++;

        if (input.files.length === fileReadCounter) {
          this.journal.analyze();
        }
      };
    }
  }

  public runs() {
    return this.journal.miningRuns.filter((value) => {
      if (this.onlyWithCollection && value.launchedCollections === 0) {
        return false;
      }
      return !(this.onlyWithProspector && value.launchedProspectors === 0);
    }).reverse();
  }

  public contribute(run: MiningRun) {
    console.log(run.id, run.location);

    const dialogRef = this.bottomSheet.open(ContributeComponent, {
      data: {
        run,
      },
    });
    dialogRef.instance.run = run;
    console.log(dialogRef);


  }
}
