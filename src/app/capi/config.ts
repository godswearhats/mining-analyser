import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  // Url of the Identity Provider
  issuer: 'https://auth.frontierstore.net',

  loginUrl: 'https://auth.frontierstore.net/auth',
  tokenEndpoint: 'https://auth.frontierstore.net/token',

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin + window.location.pathname,

  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId: 'e2d61c3b-361b-4a0b-a267-68766e6f4aec',

  responseType: 'code',

  oidc: false,

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'auth capi',

  showDebugInformation: true,
};
