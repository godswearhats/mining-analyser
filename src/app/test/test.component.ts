/* tslint:disable:object-literal-key-quotes quotemark whitespace */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  public sliderValue = 0;
  public results: any = [];

  constructor() {
    this.updateChart();
  }

  ngOnInit(): void {
  }

  public percentTickFormatting(val: any): string {
    return val.toLocaleString('en-uk', {style: 'percent', maximumSignificantDigits: 1});
  }

  public updateChart() {
    switch (this.sliderValue) {
      case 0:
        this.results = [{"name": 38, "value": 0}, {"name": 37, "value": 0.001987789294}, {"name": 36, "value": 0.00887405935}, {"name": 35, "value": 0.01838705097}, {"name": 34, "value": 0.03002981684}, {"name": 33, "value": 0.0425244924}, {"name": 32, "value": 0.05331534857}, {"name": 31, "value": 0.06524208434}, {"name": 30, "value": 0.08199630839}, {"name": 29, "value": 0.1017322164}, {"name": 28, "value": 0.1231009513}, {"name": 27, "value": 0.1462444981}, {"name": 26, "value": 0.1693170524}, {"name": 25, "value": 0.1935964788}, {"name": 24, "value": 0.2149652137}, {"name": 23, "value": 0.2315774528}, {"name": 22, "value": 0.2461309101}, {"name": 21, "value": 0.2587675706}, {"name": 20, "value": 0.2732500355}, {"name": 19, "value": 0.2911401391}, {"name": 18, "value": 0.3081783331}, {"name": 17, "value": 0.3232997302}, {"name": 16, "value": 0.3401959392}, {"name": 15, "value": 0.3560982536}, {"name": 14, "value": 0.3746272895}, {"name": 13, "value": 0.3897486866}, {"name": 12, "value": 0.4031662644}, {"name": 11, "value": 0.418358654}, {"name": 10, "value": 0.4366747125}, {"name": 9, "value": 0.4600312367}, {"name": 8, "value": 0.4880022718}, {"name": 7, "value": 0.5204458327}, {"name": 6, "value": 0.5540962658}, {"name": 5, "value": 0.5894505182}, {"name": 4, "value": 0.6255856879}, {"name": 3, "value": 0.6561834446}, {"name": 2, "value": 0.6766292773}, {"name": 1, "value": 0.6840834872}, {"name": 0, "value": 0.6842254721}];
        break;
      case 20:
        this.results = [{"name": 34,"value": 0},{"name": 33,"value": 0.001300390117},{"name": 32,"value": 0.003901170351},{"name": 31,"value": 0.006501950585},{"name": 30,"value": 0.007802340702},{"name": 29,"value": 0.01040312094},{"name": 28,"value": 0.01300390117},{"name": 27,"value": 0.01690507152},{"name": 26,"value": 0.02340702211},{"name": 25,"value": 0.03120936281},{"name": 24,"value": 0.04031209363},{"name": 23,"value": 0.0455136541},{"name": 22,"value": 0.05461638492},{"name": 21,"value": 0.0611183355},{"name": 20,"value": 0.0689206762},{"name": 19,"value": 0.07542262679},{"name": 18,"value": 0.08322496749},{"name": 17,"value": 0.08972691808},{"name": 16,"value": 0.09362808843},{"name": 15,"value": 0.09622886866},{"name": 14,"value": 0.09882964889},{"name": 13,"value": 0.1079323797},{"name": 12,"value": 0.1118335501},{"name": 11,"value": 0.122236671},{"name": 10,"value": 0.1287386216},{"name": 9,"value": 0.1430429129},{"name": 8,"value": 0.1599479844},{"name": 7,"value": 0.1807542263},{"name": 6,"value": 0.200260078},{"name": 5,"value": 0.2093628088},{"name": 4,"value": 0.2275682705},{"name": 3,"value": 0.2431729519},{"name": 2,"value": 0.2496749025},{"name": 1,"value": 0.2535760728},{"name": 0,"value": 0.2535760728}];
        break;
      case 40:
        this.results = [{"name": 32,"value": 0},{"name": 29,"value": 0.002638522427},{"name": 28,"value": 0.01300390117},{"name": 26,"value": 0.007915567282},{"name": 25,"value": 0.01055408971},{"name": 24,"value": 0.01583113456},{"name": 22,"value": 0.01846965699},{"name": 20,"value": 0.02638522427},{"name": 19,"value": 0.03693931398},{"name": 18,"value": 0.04221635884},{"name": 17,"value": 0.05013192612},{"name": 16,"value": 0.06068601583},{"name": 15,"value": 0.06596306069},{"name": 14,"value": 0.06860158311},{"name": 12,"value": 0.07124010554},{"name": 10,"value": 0.07915567282},{"name": 9,"value": 0.08443271768},{"name": 8,"value": 0.09498680739},{"name": 7,"value": 0.1029023747},{"name": 6,"value": 0.1134564644},{"name": 5,"value": 0.1266490765},{"name": 4,"value": 0.1451187335},{"name": 3,"value": 0.1530343008},{"name": 0,"value": 0.1609498681}];
        break;
      case 60:
        this.results = [{"name": 2, "value": 0.10800744878957169}, {"name": 3, "value": 0.10117939168218498}, {"name": 4, "value": 0.09124767225325885}, {"name": 5, "value": 0.08504034761018}, {"name": 6, "value": 0.07759155803848541}, {"name": 7, "value": 0.07014276846679081}, {"name": 8, "value": 0.06517690875232775}, {"name": 9, "value": 0.061452513966480445}, {"name": 10, "value": 0.05834885164494103}, {"name": 11, "value": 0.055865921787709494}, {"name": 12, "value": 0.054003724394785846}, {"name": 13, "value": 0.051520794537554315}, {"name": 14, "value": 0.04903786468032278}, {"name": 15, "value": 0.04407200496585972}, {"name": 16, "value": 0.040347610180012414}, {"name": 17, "value": 0.037243947858473}, {"name": 18, "value": 0.03414028553693358}, {"name": 19, "value": 0.029174425822470516}, {"name": 20, "value": 0.0260707635009311}, {"name": 21, "value": 0.0223463687150838}, {"name": 22, "value": 0.019242706393544383}, {"name": 23, "value": 0.014276846679081317}, {"name": 24, "value": 0.012414649286157667}, {"name": 25, "value": 0.00931098696461825}, {"name": 26, "value": 0.0074487895716946}, {"name": 27, "value": 0.00558659217877095}, {"name": 28, "value": 0.0037243947858473}, {"name": 29, "value": 0.00186219739292365}, {"name": 31, "value": 0.0006207324643078833}, {"name": 34, "value": 0}];
        break;
      case 80:
        this.results = [{"name": 3, "value": 0.04289544235924933}, {"name": 4, "value": 0.0388739946380697}, {"name": 5, "value": 0.03485254691689008}, {"name": 6, "value": 0.030831099195710455}, {"name": 7, "value": 0.02680965147453083}, {"name": 8, "value": 0.024128686327077747}, {"name": 9, "value": 0.021447721179624665}, {"name": 10, "value": 0.020107238605898123}, {"name": 11, "value": 0.01742627345844504}, {"name": 12, "value": 0.014745308310991957}, {"name": 14, "value": 0.013404825737265416}, {"name": 15, "value": 0.012064343163538873}, {"name": 17, "value": 0.010723860589812333}, {"name": 18, "value": 0.00938337801608579}, {"name": 19, "value": 0.006702412868632708}, {"name": 22, "value": 0.005361930294906166}, {"name": 24, "value": 0.004021447721179625}, {"name": 29, "value": 0.0013404825737265416}, {"name": 31, "value": 0}];
        break;
      case 100:
        this.results = [];
        break;
    }

    this.results = [
      {
        name: 'x',
        series: this.results,
      }
    ];
  }
}
